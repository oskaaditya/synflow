export interface TrustedProps {
  name: string;
  url: string;
}
