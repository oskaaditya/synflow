import { IconType } from 'react-icons';

export interface LinkSynflowProps {
  title: string;
  url: string;
}

export interface SocialMedisSynflowProps {
  icon: IconType;
  url: string;
}
