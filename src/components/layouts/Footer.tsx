import React from 'react';
import { Button } from '../ui/button';
import { LinkMenuSynflow, SocialMediaFlow } from '../../app/constants';
import Link from 'next/link';
import Logo from './Logo';
import { Appear } from './Animate';

const Footer = () => {
  return (
    <footer className='pt-10'>
      <div className='container mx-auto'>
        <Appear delayTime={0.4}>
          <div className='grid items-center justify-center border-b pb-[100px] lg:grid-cols-2'>
            <h1 className='text-center text-4xl font-medium tracking-tight text-head_1 md:text-5xl md:leading-[76.8px] lg:text-start'>
              Secure your money with precision
            </h1>
            <div className='mt-5 flex items-center justify-center gap-4 lg:mt-0 lg:justify-end'>
              <Button className='rounded-full md:text-lg'>Open Account</Button>
              <Button variant={'outline'} className='rounded-full md:text-lg'>
                Learn More
              </Button>
            </div>
          </div>
        </Appear>
        <Appear delayTime={0.6}>
          <div className='grid items-center justify-center py-10 md:grid-cols-3'>
            <div className='mb-4 flex justify-center md:mb-0   md:justify-start'>
              <Logo />
            </div>
            <div className='flex items-center justify-center gap-4'>
              {LinkMenuSynflow.map((link, index) => (
                <Link
                  key={index}
                  href={link.url}
                  className='transition-all hover:text-primary'
                >
                  {link.title}
                </Link>
              ))}
            </div>
            <div className='mt-4 flex items-center justify-center gap-4 md:mt-0 md:justify-end'>
              {SocialMediaFlow.map((item, index) => (
                <Link
                  key={index}
                  href={item.url}
                  className='text-2xl text-[#546078] transition-all hover:text-primary'
                  target='_blank'
                >
                  {React.createElement(item.icon)}
                </Link>
              ))}
            </div>
          </div>
        </Appear>
      </div>
    </footer>
  );
};

export default Footer;
