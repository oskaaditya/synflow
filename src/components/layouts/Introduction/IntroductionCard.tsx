import React, { FC } from 'react';
import { Card, CardContent, CardHeader, CardTitle } from '../../ui/card';
import { Button } from '../../ui/button';
import { Plus } from 'lucide-react';

interface IntroductionCardProps {
  title: string;
  children?: React.ReactNode;
}

const IntroductionCard: FC<IntroductionCardProps> = ({ title, children }) => {
  return (
    <Card className='px-0s border-none py-5 xl:px-12 xl:py-10'>
      <CardHeader className='flex flex-row items-start justify-between'>
        <CardTitle className='max-w-[80%] text-2xl text-primary md:max-w-xs md:text-4xl'>
          {title}
        </CardTitle>
        <Button
          variant={'ghost'}
          className='w-fit rounded-full bg-primary p-2 text-sm text-white hover:bg-primary/90 md:text-base'
        >
          <Plus className='text-white' size={16} />
        </Button>
      </CardHeader>
      <CardContent>{children}</CardContent>
    </Card>
  );
};

export default IntroductionCard;
