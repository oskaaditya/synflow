import { Progress } from '@nextui-org/react';
import React from 'react';

const ProggressBar = () => {
  return (
    <div className='relative mb-12'>
      <Progress
        size='md'
        radius='none'
        classNames={{
          base: 'max-w-full',
          track: 'drop-shadow-md bg-[#68BABB]/30',
          indicator: 'bg-gradient-to-r from-[#154043] to-[#6EC3C4]',
          value: 'text-foreground/60',
        }}
        value={80}
      />
      <div
        className={`control animate-fade-in absolute -top-2.5 right-[18%] bg-white px-1`}
      >
        <div className='h-8 w-4 rounded-full border-4 border-[#68BABB]'></div>
      </div>
    </div>
  );
};

export default ProggressBar;
