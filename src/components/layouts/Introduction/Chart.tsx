import { cn } from '@/lib/utils';
import React, { FC } from 'react';

interface ChartProps {
  value?: string;
  date?: string;
  empty?: boolean;
  className1?: string;
  className2?: string;
}

const Chart: FC<ChartProps> = ({ value, date, className1, className2 }) => {
  return (
    <div className='chart '>
      <p className='mb-[10px] text-center text-[#8793AB]'>
        {value ? value : 'Empty'}
      </p>
      <div
        className={cn(
          'flex w-full items-end rounded-lg bg-[#E2F3F3] p-2',
          className1
        )}
      >
        <div className={cn('w-full rounded-lg bg-[#6EC3C4]', className2)}></div>
      </div>
      <p className='mt-2 text-center text-xs text-[#546078]'>{date}</p>
    </div>
  );
};

export default Chart;
