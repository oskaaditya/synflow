import {
  Navbar,
  NavbarBrand,
  NavbarContent,
  NavbarItem,
  NavbarMenu,
  NavbarMenuItem,
  NavbarMenuToggle,
} from '@nextui-org/react';
import React from 'react';
import Logo from './Logo';
import Link from 'next/link';
import { LinkMenuSynflow } from '../../app/constants';
import { Button } from '../ui/button';
import { ChevronRight } from 'lucide-react';

const NavbarComponent = () => {
  return (
    <Navbar isBordered className='bg-white py-4'>
      <div className='container mx-auto flex items-center justify-between md:justify-center'>
        <NavbarContent className='pr-3 lg:hidden' justify='start'>
          <NavbarBrand>
            <Logo />
          </NavbarBrand>
        </NavbarContent>

        <NavbarContent className='hidden gap-4 lg:flex' justify='start'>
          <Logo />
          {LinkMenuSynflow.map((item, index) => (
            <NavbarMenuItem key={`${item}-${index}`}>
              <Link className='w-full' href={item.url}>
                {item.title}
              </Link>
            </NavbarMenuItem>
          ))}
        </NavbarContent>

        <NavbarContent justify='end'>
          <NavbarItem className='hidden lg:flex'>
            <Button
              variant={'outline'}
              className='rounded-full font-medium'
              size={'md'}
            >
              Login
            </Button>
          </NavbarItem>
          <NavbarItem className='hidden lg:flex'>
            <Button className='rounded-full' size={'md'}>
              Sign Up <ChevronRight />
            </Button>
          </NavbarItem>
        </NavbarContent>

        <NavbarMenu className='w-full items-center bg-white'>
          {LinkMenuSynflow.map((item, index) => (
            <NavbarMenuItem key={`${item}-${index}`}>
              <Link className='w-full' href={item.url}>
                {item.title}
              </Link>
            </NavbarMenuItem>
          ))}
          <NavbarItem>
            <Button
              variant={'outline'}
              className='w-full rounded-full'
              size={'md'}
            >
              Login
            </Button>
          </NavbarItem>
          <NavbarItem>
            <Button className='rounded-full' size={'md'}>
              Sign Up <ChevronRight />
            </Button>
          </NavbarItem>
        </NavbarMenu>

        <NavbarContent className='lg:hidden' justify='end'>
          <NavbarMenuToggle />
        </NavbarContent>
      </div>
    </Navbar>
  );
};

export default NavbarComponent;
