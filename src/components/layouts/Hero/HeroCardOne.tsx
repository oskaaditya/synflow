import { Card, CardContent } from '@/components/ui/card';
import Image from 'next/image';
import React, { FC } from 'react';

interface HeroCardOneProps {
  tag: string;
  title: string;
}

const HeroCardOne: FC<HeroCardOneProps> = ({ tag, title }) => {
  return (
    <Card className='relative float-none w-full overflow-hidden rounded-3xl bg-[#154043] md:float-right lg:w-[505px] lg:max-w-[505px]'>
      <div className='max-h-[208px] bg-card-hero-pattern-1 bg-cover bg-bottom'>
        <Image
          src={
            'https://res.cloudinary.com/dysce4sh2/image/upload/v1700995336/duh5n1kvpskoaopwgild.png'
          }
          width={560}
          height={560}
          quality={100}
          alt='main-card-1'
          className='h-auto w-full'
        />
      </div>
      <CardContent className='bg-[#154043]'>
        <div className='p-2'>
          <p className='text-xl text-white'>{tag}</p>
          <h2 className='max-w-xs text-2xl text-[#6EC3C4] md:text-[32px]'>
            {title}
          </h2>
        </div>
      </CardContent>
    </Card>
  );
};

export default HeroCardOne;
