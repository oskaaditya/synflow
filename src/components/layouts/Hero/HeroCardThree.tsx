import { Card, CardContent } from '@/components/ui/card';
import Image from 'next/image';
import React, { FC } from 'react';
import { ArrowUp } from 'lucide-react';

interface HeroCardThreeProps {
  tag: string;
  revenue: string;
  percentage: number;
}

const HeroCardThree: FC<HeroCardThreeProps> = ({
  tag,
  revenue,
  percentage,
}) => {
  return (
    <Card className='relative w-full overflow-hidden rounded-3xl border-none bg-[#6EC3C433] md:max-h-[356px] lg:min-w-[348px] lg:max-w-[348px]'>
      <CardContent className='p-4 pb-0'>
        <div className=''>
          <p className='text-xl text-primary'>{tag}</p>
          <h2 className='text-[48px] text-primary'>
            ${revenue}
            <span className='text-2xl font-medium'> USD</span>
          </h2>
          <p className='flex items-center gap-1 text-2xl font-medium text-[#3B9091]'>
            <ArrowUp /> {percentage}%
          </p>
        </div>
        <div className='-mr-4 mt-8'>
          <Image
            src={
              'https://res.cloudinary.com/dysce4sh2/image/upload/v1701006384/uuzxgdlbnwzrwi4rmng4.png'
            }
            alt='chart'
            width={366}
            height={199}
            quality={100}
            className='float-right h-auto w-full max-w-[344px]'
          />
        </div>
      </CardContent>
    </Card>
  );
};

export default HeroCardThree;
