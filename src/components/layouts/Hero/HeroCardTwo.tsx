import { Card, CardContent } from '@/components/ui/card';
import Image from 'next/image';
import React, { FC } from 'react';

interface HeroCardTwoProps {
  tag: string;
  title: string;
}

const HeroCardTwo: FC<HeroCardTwoProps> = ({ tag, title }) => {
  return (
    <Card className='w-full overflow-hidden rounded-3xl bg-gradient-to-b from-[#49B4B6] to-[#6EC3C4] md:h-full lg:min-w-[360px] lg:max-w-[360px]'>
      <div className='relative max-w-[300px] p-5'>
        <Image
          src={
            'https://res.cloudinary.com/dysce4sh2/image/upload/v1700998727/ijtwe0tkx9anrd753sbi.png'
          }
          width={219}
          height={200}
          quality={100}
          className='h-auto w-full max-w-[200px] mix-blend-luminosity'
          alt='coins'
        />
        <Image
          src={
            'https://res.cloudinary.com/dysce4sh2/image/upload/v1700998727/vakolhsyo5h44su6mgki.png'
          }
          width={299}
          height={148}
          quality={100}
          className='absolute left-0 top-[20%]'
          alt='circle'
        />
        <Image
          src={
            'https://res.cloudinary.com/dysce4sh2/image/upload/v1700998727/so3rqb9ivvavxoixgvm6.png'
          }
          width={42}
          height={54}
          quality={100}
          className='absolute right-0 top-[22%]'
          alt='star'
        />
      </div>
      <CardContent>
        <div className='p-2'>
          <p className='text-xl text-white'>{tag}</p>
          <h2 className='text-3xl text-white md:text-2xl'>{title}</h2>
        </div>
      </CardContent>
    </Card>
  );
};

export default HeroCardTwo;
