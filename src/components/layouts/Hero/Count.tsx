import React, { FC } from 'react';
import CountUp from 'react-countup';

interface CountProps {
  value: number;
  descriptions?: string;
  currency?: boolean;
  total?: boolean;
  totalName?: '+' | 'B';
  useDecimal?: boolean;
  totalDecimal?: number;
  decimalType?: '.' | ',';
}

const Count: FC<CountProps> = ({
  value,
  descriptions,
  currency,
  total,
  totalName,
  useDecimal,
  totalDecimal,
  decimalType,
}) => {
  return (
    <div>
      <h2 className='text-4xl font-medium text-primary md:text-[52px]'>
        <span>
          {currency && '$'}
          <CountUp
            start={0}
            end={value}
            duration={2}
            decimals={useDecimal ? totalDecimal : 0}
            decimal={decimalType}
          />
          {total && totalName}
        </span>
      </h2>
      <p className='mt-3 max-w-[400px] text-sm text-[#546078] md:text-xl'>
        {descriptions}
      </p>
    </div>
  );
};

export default Count;
