import { Appear } from '@/components/layouts/Animate';
import Image from 'next/image';
import React from 'react';

const PhoneCard = () => {
  return (
    <Appear delayTime={0.5}>
      <div className='relative flex justify-center bg-cta-pattern bg-cover md:h-[600px]'>
        <Image
          src={
            'https://res.cloudinary.com/dysce4sh2/image/upload/v1700920171/x9djdhmf3she28i211fe.png'
          }
          quality={100}
          width={568}
          height={768}
          alt='phone'
          className='h-[384px] max-h-[768px] w-[284px] max-w-[568px] md:h-auto md:w-full'
        />

        <Image
          src={
            'https://res.cloudinary.com/dysce4sh2/image/upload/v1700919888/gqftg53pmjbl0m9pn0fr.png'
          }
          quality={100}
          width={739.96}
          height={637.43}
          alt='card'
          className='absolute h-auto max-h-[637.43px] w-full max-w-[470px] mix-blend-luminosity md:max-w-[739.96px]'
        />
      </div>
    </Appear>
  );
};

export default PhoneCard;
