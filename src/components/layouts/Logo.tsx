import Image from 'next/image';
import React from 'react';

const Logo = () => {
  return (
    <Image
      src={
        'https://res.cloudinary.com/dysce4sh2/image/upload/v1700827883/zqet4kdbqtwhwluwdguk.png'
      }
      quality={100}
      width={100}
      height={100}
      alt='logo-synflow'
      className='h-auto w-[135px]'
    />
  );
};

export default Logo;
