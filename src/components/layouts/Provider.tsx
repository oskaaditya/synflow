'use client';
import React, { FC } from 'react';

import { NextUIProvider } from '@nextui-org/react';

interface ProviderProps {
  children: React.ReactNode;
}

const Provider: FC<ProviderProps> = ({ children }) => {
  return <NextUIProvider>{children}</NextUIProvider>;
};

export default Provider;
