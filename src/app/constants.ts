import { AccordionProps } from '@/interfaces/accordion';
import { LinkSynflowProps, SocialMedisSynflowProps } from '@/interfaces/link';
import { TrustedProps } from '@/interfaces/trusted';
import { FaTwitter, FaLinkedin, FaFacebook } from 'react-icons/fa';

export const LinkMenuSynflow: Array<LinkSynflowProps> = [
  {
    title: 'Solutions',
    url: '#solutions',
  },
  {
    title: 'Product',
    url: '#product',
  },
  {
    title: 'Company',
    url: '#company',
  },
  {
    title: 'Insight',
    url: '#insight',
  },
];

export const SocialMediaFlow: Array<SocialMedisSynflowProps> = [
  {
    icon: FaTwitter,
    url: 'https://twitter.com/',
  },
  {
    icon: FaLinkedin,
    url: 'https://www.linkedin.com/',
  },
  {
    icon: FaFacebook,
    url: 'https://www.facebook.com/',
  },
];

export const AccordionListSynflow: Array<AccordionProps> = [
  {
    title: 'How will the money be received',
    description:
      'When you send an international money transfer, we’ll simply deposit the money into your recipient’s bank account — unless you’ve selected a method where this works differently, like Interac for CAD, for example.',
  },
  {
    title: 'What is an international money transfer',
    description:
      'When you send an international money transfer, we’ll simply deposit the money into your recipient’s bank account — unless you’ve selected a method where this works differently, like Interac for CAD, for example.',
  },
  {
    title: 'How do I send money to a bank account',
    description:
      'When you send an international money transfer, we’ll simply deposit the money into your recipient’s bank account — unless you’ve selected a method where this works differently, like Interac for CAD, for example.',
  },
  {
    title: 'How do I verify my identity',
    description:
      'When you send an international money transfer, we’ll simply deposit the money into your recipient’s bank account — unless you’ve selected a method where this works differently, like Interac for CAD, for example.',
  },
];

export const TrustedList: Array<TrustedProps> = [
  {
    name: 'Loom',
    url: 'https://res.cloudinary.com/dysce4sh2/image/upload/v1700993759/cwpbel8puo738yhhtm2o.svg',
  },
  {
    name: 'Hubspot',
    url: 'https://res.cloudinary.com/dysce4sh2/image/upload/v1700993759/wlmxabthhjowvji6ro0c.svg',
  },
  {
    name: 'Ramp',
    url: 'https://res.cloudinary.com/dysce4sh2/image/upload/v1700993759/mngwynjgkoc7uuegrrza.svg',
  },
];
