import localFont from 'next/font/local';

export const roobert = localFont({
  src: [
    {
      path: '../fonts/Roobert-Regular.otf',
      weight: '400',
    },
    {
      path: '../fonts/Roobert-Medium.otf',
      weight: '500',
    },
  ],
});
