import type { Metadata } from 'next';

import './globals.css';
import Navbar from '@/components/layouts/Navbar';
import Footer from '@/components/layouts/Footer';
import { roobert } from './fonts';
import Provider from '@/components/layouts/Provider';

export const metadata: Metadata = {
  title: 'Synlow',
  description: 'Secure your money with precision',
};

export default function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <html lang='en'>
      <body className={`${roobert.className}`}>
        <Provider>
          <Navbar />
          {children}
          <Footer />
        </Provider>
      </body>
    </html>
  );
}
