'use client';
import {
  Accordion,
  AccordionContent,
  AccordionItem,
  AccordionTrigger,
} from '@/components/ui/accordion';
import Link from 'next/link';
import { AccordionListSynflow, TrustedList } from './constants';
import { useState } from 'react';
import Image from 'next/image';
import { Button } from '@/components/ui/button';
import PhoneCard from '@/components/layouts/CTA/PhoneCard';
import IntroductionCard from '@/components/layouts/Introduction/IntroductionCard';
import ProggressBar from '@/components/layouts/Introduction/ProggressBar';
import Count from '@/components/layouts/Hero/Count';
import HeroCardOne from '@/components/layouts/Hero/HeroCardOne';
import HeroCardTwo from '@/components/layouts/Hero/HeroCardTwo';
import HeroCardThree from '@/components/layouts/Hero/HeroCardThree';
import Chart from '@/components/layouts/Introduction/Chart';
import { Appear, Zoom } from '@/components/layouts/Animate';

export default function Home() {
  const [openIndex, setOpenIndex] = useState<number | null>(null);

  const handleToggle = (index: number) => {
    setOpenIndex((prevIndex) => (prevIndex === index ? null : index));
  };
  return (
    <main>
      <section className='py-[60px] md:px-10 lg:py-[100px]'>
        <div className='container mx-auto '>
          <div className='grid grid-cols-1 xl:grid-cols-2'>
            <div className='flex flex-col'>
              <div>
                <Appear>
                  <h1 className='text-4xl font-medium tracking-tight text-head_1 md:text-7xl'>
                    Secure your money with precision
                  </h1>
                </Appear>
                <Appear delayTime={0.2}>
                  <p className='mb-8 mt-6 max-w-[400px]  text-[#546078] md:text-xl'>
                    Join over million people who choose synflow for fast and
                    secure future banking.
                  </p>
                </Appear>
                <Appear delayTime={0.4}>
                  <Button className='rounded-full md:text-lg'>
                    Open Account
                  </Button>
                </Appear>
              </div>
              <Appear delayTime={0.6}>
                <div className='mb-4 mt-[56px] flex flex-wrap items-center gap-4 md:gap-[56px] xl:mb-[200px]'>
                  <Count
                    value={140}
                    total
                    descriptions='Total Currencies'
                    totalName='+'
                  />
                  <Count
                    value={1.2}
                    total
                    totalName='B'
                    currency
                    useDecimal
                    totalDecimal={1}
                    descriptions='Revenue Generated'
                  />
                </div>
              </Appear>
              <Appear delayTime={0.8}>
                <p className='mb-8 mt-6 max-w-[400px] uppercase text-[#546078] md:text-xl'>
                  Trusted by The best
                </p>
                <div className='flex flex-wrap items-center gap-4 md:gap-12'>
                  {TrustedList.map((item, index) => (
                    <Image
                      key={index}
                      src={item.url}
                      width={100}
                      height={100}
                      alt={item.name}
                      className='h-auto w-full max-w-[80px] md:max-w-[102px]'
                    />
                  ))}
                </div>
              </Appear>
            </div>
            <div className='mt-10 flex flex-col items-center xl:mt-0 xl:items-end'>
              <Zoom>
                <HeroCardOne
                  tag='Extra Secure'
                  title='Fraud and security keep your money safe'
                />
              </Zoom>
              <div className='mt-8 flex flex-wrap items-center gap-4 md:flex-nowrap'>
                <Zoom delayTime={0.2}>
                  <HeroCardTwo
                    tag='Currencies'
                    title='Hundreds of countries in one card'
                  />
                </Zoom>
                <Zoom delayTime={0.4}>
                  <HeroCardThree
                    tag='Growth Revenue'
                    revenue='50,240'
                    percentage={0.024}
                  />
                </Zoom>
              </div>
            </div>
          </div>
        </div>
      </section>
      {/* Section Introduction */}
      <section className='px-10 py-[60px] lg:py-[100px]'>
        <div className='container mx-auto'>
          <Appear delayTime={0.5}>
            <p className='tag font-medium uppercase tracking-widest text-[#49B4B6]'>
              Introduction
            </p>
            <div className='flex flex-wrap items-start justify-between gap-4 md:gap-0'>
              <h1 className='max-w-[558px] text-4xl font-medium tracking-tight text-head_1 md:text-5xl md:leading-[76.8px]'>
                Make your money move so fast
              </h1>
              <p className='max-w-[400px] text-xl text-[#546078]'>
                Looking for an app to send money abroad? Send & receive money –
                all in one app.
              </p>
            </div>
          </Appear>
          <Zoom delayTime={0.5}>
            <div className='mt-16 grid w-full gap-8 lg:grid-cols-2'>
              <IntroductionCard title='Quick Banking Transfers'>
                <div className='my mt-8 flex flex-col md:mt-14 md:px-7 md:py-6'>
                  <p className='text-base text-[#546078]'>Transfer</p>
                  <h2 className='mb-8 text-4xl font-medium text-primary md:mb-[84px] md:text-5xl md:leading-[76.8px]'>
                    $320,870
                  </h2>
                  <div>
                    <ProggressBar />
                  </div>
                  <div className='flex items-center justify-between border-t py-4 md:py-6'>
                    <p className='text-xs text-[#546078] md:text-base'>
                      Your payment
                    </p>
                    <p className='text-xs text-[#546078] md:text-base'>
                      2.5% of sales
                    </p>
                  </div>
                </div>
              </IntroductionCard>
              <IntroductionCard title='Powerful Annual Reporting'>
                <div className='mt-8 flex flex-col md:px-7 md:py-6'>
                  <p className='text-base text-[#546078]'>Report</p>
                  <h2 className='mb-6 text-[32px] font-medium text-primary '>
                    Aug 14, 2023
                  </h2>
                  <div className='mt-6 grid grid-cols-3 items-end gap-4'>
                    <Chart
                      date='Aug 12'
                      value='$120'
                      className1='h-[100px]'
                      className2='h-[28px]'
                    />
                    <Chart date='Aug 13' empty />
                    <Chart
                      date='Today'
                      value='$165'
                      className1='h-[143px]'
                      className2='h-[13px]'
                    />
                  </div>
                </div>
              </IntroductionCard>
            </div>
          </Zoom>
        </div>
      </section>
      {/* Section CTA */}
      <section className='overflow-hidden bg-primary pt-[60px] lg:pt-[100px]'>
        <div className='container mx-auto'>
          <div className='flex flex-col items-center justify-center'>
            <Zoom delayTime={0.5}>
              <Image
                src={
                  'https://res.cloudinary.com/dysce4sh2/image/upload/v1700918135/vzxidrpz2i4rjqlhysuq.png'
                }
                width={200}
                height={180}
                alt='cta-img'
              />
            </Zoom>
            <Appear delayTime={0.5}>
              <h1 className='-mt-10 text-center text-4xl font-medium tracking-tight text-head_1  text-white md:text-5xl md:leading-[76.8px]'>
                Pay securely with digital cards
              </h1>
            </Appear>
            <Appear delayTime={0.6}>
              <p className='my-6 max-w-[560px] text-center text-white/70 md:text-xl'>
                Create virtual cards to separate your expensetypes. No waiting
                around for delivery.
              </p>
            </Appear>
            <Appear delayTime={0.7}>
              <Button className='mb-[106px] rounded-full' variant={'secondary'}>
                Get your card
              </Button>
            </Appear>
          </div>
        </div>
        <PhoneCard />
      </section>
      {/* Section FAQ */}
      <section className='px-10 py-[60px] lg:py-[100px]'>
        <div className='container mx-auto'>
          <div className='flex flex-col items-center justify-center'>
            <Appear delayTime={0.4}>
              <p className='tag font-medium text-[#49B4B6]'>FAQ</p>
            </Appear>
            <Appear delayTime={0.6}>
              <h1 className='max-w-[600px] text-center text-4xl font-medium tracking-tight  text-head_1 md:text-5xl md:leading-[76.8px]'>
                Frequently asked question
              </h1>
            </Appear>
            <Appear delayTime={0.8}>
              <p className='mt-6 text-center md:text-start'>
                Visit our{' '}
                <span>
                  <Link
                    href='#'
                    className='font-semibold text-primary underline'
                  >
                    Help Center
                  </Link>
                </span>{' '}
                for more Information.
              </p>
            </Appear>
          </div>
          <Appear delayTime={1}>
            <div className='mx-auto mt-8 max-w-[800px]'>
              <Accordion type='single' collapsible className='w-full'>
                {AccordionListSynflow.map((item, index) => (
                  <AccordionItem key={index} value={`item-${index}`}>
                    <AccordionTrigger
                      index={index}
                      isOpen={openIndex === index}
                      onToggle={() => handleToggle(index)}
                    >
                      <p className='acc-title font-medium'>
                        {item.title}
                        <span>?</span>
                      </p>
                    </AccordionTrigger>
                    <AccordionContent className='text-md text-[#546078] md:text-base'>
                      {item.description}
                    </AccordionContent>
                  </AccordionItem>
                ))}
              </Accordion>
            </div>
          </Appear>
        </div>
      </section>
    </main>
  );
}
